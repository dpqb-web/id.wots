sisi = Tilt::SlimTemplate.new{"
sass:
	:root
		--latar-warna: #08f
		// --latar-gambar: url()
		// --latar-ulang: repeat
section
	main
		img src='#{Faker::LoremFlickr.image(size: "81x81")}' alt='wajah'
		.rincian-saluran
			.nama title='#{Faker::Name.name}' = Faker::Name.name
			.tagar title='#{Faker::Internet.username}' = Faker::Internet.username
			form action='saluran.xyz.html' method='get'
				input type='hidden' name='ikut' value='1'
				input type='hidden' name='tagar' value='#{Faker::Internet.username}'
				button type='submit' Ikuti
section
	header
		.judul
			| Tentang 
			= Faker::Name.name
	main
		- for i in Faker::Lorem.paragraphs
			p = i
"}

File.write('_sisi.html', sisi.render)

hal_depan = Tilt::SlimTemplate.new{"
article
	section
		header
			menu.tab
				li.hidup
					a href='saluran.xyz.html' Terbitan
				li
					a href='saluran.xyz.video.html' Video
				li
					a href='saluran.xyz.daftar_putar.html' Daftar putar
				li
					a href='saluran.xyz.pengikut.html' Pengikut
				li
					a href='saluran.xyz.diikuti.html' Diikuti
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/saluran.xyz.html', $dasar.render{hal_depan.render})

hal_video = Tilt::SlimTemplate.new{"
article
	section
		header
			menu.tab
				li.hidup
					a href='saluran.xyz.html' Terbitan
				li
					a href='saluran.xyz.video.html' Video
				li
					a href='saluran.xyz.daftar_putar.html' Daftar putar
				li
					a href='saluran.xyz.pengikut.html' Pengikut
				li
					a href='saluran.xyz.diikuti.html' Diikuti
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/saluran.xyz.video.html', $dasar.render{hal_video.render})

hal_daftar_putar = Tilt::SlimTemplate.new{"
article
	section
		header
			menu.tab
				li.hidup
					a href='saluran.xyz.html' Terbitan
				li
					a href='saluran.xyz.video.html' Video
				li
					a href='saluran.xyz.daftar_putar.html' Daftar putar
				li
					a href='saluran.xyz.pengikut.html' Pengikut
				li
					a href='saluran.xyz.diikuti.html' Diikuti
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/saluran.xyz.daftar_putar.html', $dasar.render{hal_daftar_putar.render})

hal_pengikut = Tilt::SlimTemplate.new{"
article
	section
		header
			menu.tab
				li.hidup
					a href='saluran.xyz.html' Terbitan
				li
					a href='saluran.xyz.video.html' Video
				li
					a href='saluran.xyz.daftar_putar.html' Daftar putar
				li
					a href='saluran.xyz.pengikut.html' Pengikut
				li
					a href='saluran.xyz.diikuti.html' Diikuti
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/saluran.xyz.pengikut.html', $dasar.render{hal_pengikut.render})

hal_diikuti = Tilt::SlimTemplate.new{"
article
	section
		header
			menu.tab
				li.hidup
					a href='saluran.xyz.html' Terbitan
				li
					a href='saluran.xyz.video.html' Video
				li
					a href='saluran.xyz.daftar_putar.html' Daftar putar
				li
					a href='saluran.xyz.pengikut.html' Pengikut
				li
					a href='saluran.xyz.diikuti.html' Diikuti
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/saluran.xyz.diikuti.html', $dasar.render{hal_diikuti.render})

daftar = Tilt::SlimTemplate.new{"
article
	section
		header
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/daftar_putar.xyz.html', $dasar.render{daftar.render})

tonton = Tilt::SlimTemplate.new{"
article
	section
		header
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside == File.read('_sisi.html')
"}

File.write('../public/video.xyz.html', $dasar.render{tonton.render})