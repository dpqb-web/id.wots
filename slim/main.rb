require 'slim'
require 'faker'

$dasar = Tilt.new('_kerangka.slim')

index = Tilt::SlimTemplate.new{"
article
	section
		header
			.judul Ramai hari ini
		main
			ul.petak-butir
				- for i in 1..6
					li
						a href='video.xyz.html' title=\"#{Faker::Book.title}\"
							img src='#{Faker::LoremFlickr.image(size: "320x200")}' alt='cuplikan'
							.judul = Faker::Book.title
							.tanggal = Faker::Time.backward
							.penonton = Faker::Number.number
						a href='saluran.xyz.html' title=\"#{Faker::Name.name}\"
							img src='#{Faker::LoremFlickr.image(size: "32x32")}' alt='wajah'
							.pengguna = Faker::Name.name
	section
		header
			.judul Baru hari ini
		main
			ul.petak-butir
				- for i in 1..6
					li
						a href='video.xyz.html' title=\"#{Faker::Book.title}\"
							img src='#{Faker::LoremFlickr.image(size: "320x200")}' alt='cuplikan'
							.judul = Faker::Book.title
							.tanggal = Faker::Time.backward
							.penonton = Faker::Number.number
						a href='saluran.xyz.html' title=\"#{Faker::Name.name}\"
							.pengguna = Faker::Name.name
aside
	section
		main
			p B
"}

File.write('../public/index.html', $dasar.render{index.render})

kosong = Tilt::SlimTemplate.new{"
article
	section
		header
			.judul Tidak ditemukan
		main
			p Halaman yang anda tuju tidak dapat ditemukan. Mungkin halaman tersebut:
			ul
				li terhapus; atau
				li disembunyikan.
aside
	section
		header
			.judul Tulisan
			.pranala Lebih lanjut
		main
			p enak
"}

File.write('../public/404.html', $dasar.render{kosong.render})

require_relative 'admin'
require_relative 'saluran'
require_relative 'jelajah'